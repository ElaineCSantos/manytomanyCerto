
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastrar Livro</title>
    </head>
    <body>
        <h1>Cadastro de livro</h1>
        <form method="POST" action="Controle">
            <input name="acao" type="hidden" value="salvarLivro">
            <input name="isbn" type="hidden" value="<c:out value="${livro.getIsbn()}" />" />
            <input name="titulo" type="text" value="<c:out value="${livro.getTitulo()}" />" />
            <select name="autor" class="frmCmb1">
            <option value="">..</option>
            <c:forEach var="autor" items="${autores}">
                <option value="${autores.id}">
                                ${autores.nome}
                </option>
            </c:forEach>
            </select>                                 
            <input type="submit" />
        </form>
    </body>
</html>

