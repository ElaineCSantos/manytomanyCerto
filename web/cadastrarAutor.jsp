
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastrar Autor</title>
    </head>
    <body>
        <h1>Cadastro de autor</h1>
        <form method="POST" action="Controle">
            <input name="acao" type="hidden" value="salvarAutor">
            <input name="id" type="hidden" value="<c:out value="${autor.getId()}" />" />
            <input name="nome" type="text" value="<c:out value="${autor.getNome()}" />" />                        
            <input name="livros" type="text" value="<c:out value="${autor.getLivros()}" />" />                        
            <input type="submit" />
        </form>
    </body>
</html>
