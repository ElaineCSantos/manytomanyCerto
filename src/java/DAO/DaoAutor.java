/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Bean.Autor;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

/**
 *
 * @author ElaineSantos
 */
public class DaoAutor {
    static SessionFactory fabrica = HibernateUtil.getSessionFactory();

    public void adicionarAutor(String nome, List livros, int id) {
        Autor autor = new Autor();
        Session session = fabrica.openSession();
        session.beginTransaction();
        session.save(autor);
        session.getTransaction().commit();
        session.close();
    }

    public Autor buscaAutor(String nome) {
        Session session = fabrica.openSession();
        Query q = session.createQuery("from Autor where nome = :nome");
        return (Autor) q.setString("nome", nome).uniqueResult();        
    }
    
    public Autor retornarAutor(int id) {
        Session session = fabrica.openSession();
        Query q = session.createQuery("from Autor where id = :id");
        return (Autor) q.setInteger("id", id).uniqueResult();
    }

    public void atualizarAutor(Autor autor) {
        Session session = fabrica.openSession();
        session.beginTransaction();
        session.update(autor);
        session.getTransaction().commit();
        session.close();
    }

    public void excluirAutor(Autor autor) {
        Session session = fabrica.openSession();
        session.beginTransaction();
        autor.getLivros().clear();
        session.delete(autor);
        session.getTransaction().commit();
        session.close();
    }

    public List<Autor> listarAutores() {
        Session session = fabrica.openSession();
        Query q = session.createQuery("from Autor");
        List lista = q.list();
        return lista;
    }

    public Autor retornarFabricante(int id) {
        Session session = fabrica.openSession();
        Query q = session.createQuery("from Autor where id = :id");
        return (Autor) q.setInteger("id", id).uniqueResult();
    }

}
