/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.Livro;
import DAO.DaoAutor;
import DAO.DaoLivro;
import controle.Acao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ElaineSantos
 */
public class cadastroLivro implements Acao{
    
    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {

        DaoAutor autor = new DaoAutor();
        DaoLivro livro = new DaoLivro();
        if (request.getParameter("isbn") == null) {
            request.setAttribute("livro", new Livro());
            request.setAttribute("autores", autor.listarAutores());
        } else {
            request.setAttribute("livro", livro.retornarLivro("isbn"));
        }
        return "cadastrarLivro.jsp";
    }
}
