/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.Autor;
import DAO.DaoAutor;
import DAO.DaoLivro;
import controle.Acao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ElaineSantos
 */
public class cadastroAutor implements Acao{
    
    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {

        DaoAutor autor = new DaoAutor();
        DaoLivro livro = new DaoLivro();
        if (request.getParameter("id") == null) {
            request.setAttribute("nome", new Autor());
            request.setAttribute("livros", livro.listarLivros());
        } else {
            request.setAttribute("autor", autor.retornarAutor(Integer.parseInt(request.getParameter("id"))));
        }
        return "cadastrarAutor.jsp";
    }
}
